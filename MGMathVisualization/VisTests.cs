﻿using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using MGMath.FrontlineTools.Voronoi;

namespace MGMathVisualization
{
    public static class VizTests
    {
        public static async Task BasicArcTest(VisState vis)
        {
            Arc testArc = new Arc(new Vector2(3, 5), DistanceFormulas.EuclideanDistance);
            float directix = -100;
            Vector2[] GeneratePoints()
            {
                
                float[] targets = Enumerable.Range(0, 800).Select(x => x - 400f).ToArray();
                var points = new Vector2[targets.Length];
                for (var i = 0; i < targets.Length; ++i)
                {
                    var target = targets[i];
                    points[i] = new Vector2(target, (float)testArc.GetYForX(target, directix));
                }

                return points;
            }

            var points = await Task.Run(GeneratePoints);
            vis.AddPoint("focus",testArc.Focus, Color.DarkBlue, 2);
            vis.AddInfiniteHorizontalLine("directix",directix,Color.Coral);
            vis.AddCurve("arc",points,Color.Firebrick);
            
        }
    }
    
    // TODO: Provide some way for the UI thread to send the signal. Maybe a callback in vis state?
    // TODO: Implement button on UI to step/continue.
    
    public class BeachlineStepDebugger : BeachlineDebugHistory
    {
        private int _scopeStartIndex = 0;
        // Setting this will unset any blocks. If set to False, the current block will end, but
        // it will block on the next message. If set to True, it will not block on debug signals until
        // blocking behavior is enabled again with BlockOnNextHook.
        private TaskCompletionSource<bool> _unblockSignal;

        private void WaitForSignal()
        {
            // Can't await because this gets called in a synchronous context.
            // This will block the thread until the task resolves.
            var block = _unblockSignal.Task.Result;
            if (!block)
            {
                _unblockSignal = new TaskCompletionSource<bool>();
            } 
        }

        public BeachlineStepDebugger() : base()
        {
            _unblockSignal = new TaskCompletionSource<bool>();
        }

        public void BlockOnNextHook()
        {
            _unblockSignal = new TaskCompletionSource<bool>();
        }

        public void Step()
        {
            _unblockSignal.SetResult(false);
        }
        
        public void Continue()
        {
            _unblockSignal.SetResult(true);
        }

        public override void PublishMessage(string message)
        {
            WaitForSignal();
        }

        public override void PublishVector(Vector2 toBePublished, string descriptor)
        {
            WaitForSignal();
        }
    }
}