﻿
namespace MGMathVisualization.Ui
{
    partial class SimpleTestDrawSpace
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drawSpaceSizeLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // drawSpaceSizeLabel
            // 
            this.drawSpaceSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.drawSpaceSizeLabel.AutoSize = true;
            this.drawSpaceSizeLabel.Location = new System.Drawing.Point(317, 0);
            this.drawSpaceSizeLabel.Name = "drawSpaceSizeLabel";
            this.drawSpaceSizeLabel.Size = new System.Drawing.Size(30, 15);
            this.drawSpaceSizeLabel.TabIndex = 0;
            this.drawSpaceSizeLabel.Text = "[x,y]";
            this.drawSpaceSizeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // SimpleTestDrawSpace
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this.drawSpaceSizeLabel);
            this.Name = "SimpleTestDrawSpace";
            this.Size = new System.Drawing.Size(350, 350);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label drawSpaceSizeLabel;
    }
}
