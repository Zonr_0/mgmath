﻿
namespace MGMathVisualization.Ui
{
    partial class MGMathVisualizationRoot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.testSelectionTree1 = new TestSelectionTree();
            this.debugVerticalControl1 = new DebugVerticalControl();
            this.activeTestGroupBox = new System.Windows.Forms.GroupBox();
            this.simpleTestDrawSpace1 = new SimpleTestDrawSpace();
            this.menuOptions = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.scalingGridToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scalingPresentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constantOriginToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activeTestGroupBox.SuspendLayout();
            this.menuOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // testSelectionTree1
            // 
            this.testSelectionTree1.Dock = System.Windows.Forms.DockStyle.Left;
            this.testSelectionTree1.Location = new System.Drawing.Point(0, 24);
            this.testSelectionTree1.MinimumSize = new System.Drawing.Size(150, 400);
            this.testSelectionTree1.Name = "testSelectionTree1";
            this.testSelectionTree1.Size = new System.Drawing.Size(150, 426);
            this.testSelectionTree1.TabIndex = 0;
            // 
            // debugVerticalControl1
            // 
            this.debugVerticalControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.debugVerticalControl1.Location = new System.Drawing.Point(600, 24);
            this.debugVerticalControl1.MinimumSize = new System.Drawing.Size(200, 400);
            this.debugVerticalControl1.Name = "debugVerticalControl1";
            this.debugVerticalControl1.Size = new System.Drawing.Size(200, 426);
            this.debugVerticalControl1.TabIndex = 1;
            // 
            // activeTestGroupBox
            // 
            this.activeTestGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.activeTestGroupBox.Controls.Add(this.simpleTestDrawSpace1);
            this.activeTestGroupBox.Location = new System.Drawing.Point(157, 24);
            this.activeTestGroupBox.Margin = new System.Windows.Forms.Padding(1, 1, 2, 1);
            this.activeTestGroupBox.Name = "activeTestGroupBox";
            this.activeTestGroupBox.Size = new System.Drawing.Size(437, 414);
            this.activeTestGroupBox.TabIndex = 2;
            this.activeTestGroupBox.TabStop = false;
            this.activeTestGroupBox.Text = "Test Draw Surface";
            // 
            // simpleTestDrawSpace1
            // 
            this.simpleTestDrawSpace1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleTestDrawSpace1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.simpleTestDrawSpace1.BackColor = System.Drawing.SystemColors.Window;
            this.simpleTestDrawSpace1.Location = new System.Drawing.Point(6, 22);
            this.simpleTestDrawSpace1.Name = "simpleTestDrawSpace1";
            this.simpleTestDrawSpace1.Size = new System.Drawing.Size(425, 386);
            this.simpleTestDrawSpace1.TabIndex = 0;
            // 
            // menuOptions
            // 
            this.menuOptions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuOptions.Location = new System.Drawing.Point(0, 0);
            this.menuOptions.Name = "menuOptions";
            this.menuOptions.Size = new System.Drawing.Size(800, 24);
            this.menuOptions.TabIndex = 3;
            this.menuOptions.Text = "Options";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(61, 20);
            this.toolStripMenuItem1.Text = "Options";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.scalingGridToolStripMenuItem,
            this.scalingPresentationToolStripMenuItem,
            this.constantOriginToolStripMenuItem});
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(196, 22);
            this.toolStripMenuItem2.Text = "Coordinate Conversion";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // scalingGridToolStripMenuItem
            // 
            this.scalingGridToolStripMenuItem.Name = "scalingGridToolStripMenuItem";
            this.scalingGridToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.scalingGridToolStripMenuItem.Text = "Scaling Grid";
            // 
            // scalingPresentationToolStripMenuItem
            // 
            this.scalingPresentationToolStripMenuItem.Name = "scalingPresentationToolStripMenuItem";
            this.scalingPresentationToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.scalingPresentationToolStripMenuItem.Text = "Scaling Presentation";
            // 
            // constantOriginToolStripMenuItem
            // 
            this.constantOriginToolStripMenuItem.Checked = true;
            this.constantOriginToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.constantOriginToolStripMenuItem.Name = "constantOriginToolStripMenuItem";
            this.constantOriginToolStripMenuItem.Size = new System.Drawing.Size(181, 22);
            this.constantOriginToolStripMenuItem.Text = "Constant Origin";
            // 
            // MGMathVisualizationRoot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.activeTestGroupBox);
            this.Controls.Add(this.debugVerticalControl1);
            this.Controls.Add(this.testSelectionTree1);
            this.Controls.Add(this.menuOptions);
            this.MainMenuStrip = this.menuOptions;
            this.Name = "MGMathVisualizationRoot";
            this.Text = "MGMath Debugger and Visualizer";
            this.activeTestGroupBox.ResumeLayout(false);
            this.menuOptions.ResumeLayout(false);
            this.menuOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TestSelectionTree testSelectionTree1;
        private DebugVerticalControl debugVerticalControl1;
        private System.Windows.Forms.GroupBox activeTestGroupBox;
        private SimpleTestDrawSpace simpleTestDrawSpace1;
        private System.Windows.Forms.MenuStrip menuOptions;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem scalingGridToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem scalingPresentationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constantOriginToolStripMenuItem;
    }
}