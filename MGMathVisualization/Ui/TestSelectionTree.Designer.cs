﻿
namespace MGMathVisualization.Ui
{
    partial class TestSelectionTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TestSelectionTreeView = new System.Windows.Forms.TreeView();
            this.testListLabel = new System.Windows.Forms.Label();
            this.testSelectionTreeLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.testSelectionTreeButtonTable = new System.Windows.Forms.TableLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.testSelectionTreeLayoutPanel.SuspendLayout();
            this.testSelectionTreeButtonTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // TestSelectionTreeView
            // 
            this.TestSelectionTreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TestSelectionTreeView.Location = new System.Drawing.Point(3, 18);
            this.TestSelectionTreeView.Name = "TestSelectionTreeView";
            this.TestSelectionTreeView.Size = new System.Drawing.Size(147, 346);
            this.TestSelectionTreeView.TabIndex = 0;
            // 
            // testListLabel
            // 
            this.testListLabel.AutoSize = true;
            this.testListLabel.Location = new System.Drawing.Point(3, 0);
            this.testListLabel.Name = "testListLabel";
            this.testListLabel.Size = new System.Drawing.Size(83, 15);
            this.testListLabel.TabIndex = 1;
            this.testListLabel.Text = "Available Tests";
            this.testListLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.testListLabel.Click += new System.EventHandler(this.testListLabel_Click);
            // 
            // testSelectionTreeLayoutPanel
            // 
            this.testSelectionTreeLayoutPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.testSelectionTreeLayoutPanel.ColumnCount = 1;
            this.testSelectionTreeLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.testSelectionTreeLayoutPanel.Controls.Add(this.TestSelectionTreeView, 1, 0);
            this.testSelectionTreeLayoutPanel.Controls.Add(this.testListLabel, 0, 0);
            this.testSelectionTreeLayoutPanel.Controls.Add(this.testSelectionTreeButtonTable, 0, 2);
            this.testSelectionTreeLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.testSelectionTreeLayoutPanel.Name = "testSelectionTreeLayoutPanel";
            this.testSelectionTreeLayoutPanel.RowCount = 3;
            this.testSelectionTreeLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.testSelectionTreeLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.testSelectionTreeLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.testSelectionTreeLayoutPanel.Size = new System.Drawing.Size(150, 400);
            this.testSelectionTreeLayoutPanel.TabIndex = 3;
            this.testSelectionTreeLayoutPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // testSelectionTreeButtonTable
            // 
            this.testSelectionTreeButtonTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.testSelectionTreeButtonTable.ColumnCount = 3;
            this.testSelectionTreeButtonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.testSelectionTreeButtonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.testSelectionTreeButtonTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.testSelectionTreeButtonTable.Controls.Add(this.button1, 1, 0);
            this.testSelectionTreeButtonTable.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.testSelectionTreeButtonTable.Location = new System.Drawing.Point(3, 370);
            this.testSelectionTreeButtonTable.Name = "testSelectionTreeButtonTable";
            this.testSelectionTreeButtonTable.RowCount = 1;
            this.testSelectionTreeButtonTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.testSelectionTreeButtonTable.Size = new System.Drawing.Size(147, 27);
            this.testSelectionTreeButtonTable.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(26, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Launch Test";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // TestSelectionTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.testSelectionTreeLayoutPanel);
            this.MinimumSize = new System.Drawing.Size(150, 400);
            this.Name = "TestSelectionTree";
            this.Size = new System.Drawing.Size(150, 400);
            this.testSelectionTreeLayoutPanel.ResumeLayout(false);
            this.testSelectionTreeLayoutPanel.PerformLayout();
            this.testSelectionTreeButtonTable.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView TestSelectionTreeView;
        private System.Windows.Forms.Label testListLabel;
        private System.Windows.Forms.TableLayoutPanel testSelectionTreeLayoutPanel;
        private System.Windows.Forms.TableLayoutPanel testSelectionTreeButtonTable;
        private System.Windows.Forms.Button button1;
    }
}
