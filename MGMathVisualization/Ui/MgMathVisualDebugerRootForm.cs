﻿using System;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;

namespace MGMathVisualization.Ui
{
    public partial class MgMathVisualDebuggerRootForm : Form
    {
        private VisState _visState;
        public MgMathVisualDebuggerRootForm(VisState visState)
        {
            InitializeComponent();
            _visState = visState;
        }

        protected override async void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            await VizTests.BasicArcTest(_visState);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            var origin = new Vector2(400, 200);
            var boundries = new Vector2(800, 400);
            var man = new CoordinateManager(origin);
            var g = e.Graphics;
            foreach (VisComponent item in _visState)
            {
                var pen = new Pen(item.ItemColor, item.Width);
                var gPoints = item.Points.Select(x => man.CtoG(x)).ToList();
                switch (item.ItemType)
                {
                    case VisComponentType.Point:
                        g.DrawEllipse(pen, gPoints.First().X, gPoints.First().Y,3,3);
                        break;
                    case VisComponentType.Line:
                        g.DrawLine(pen,gPoints[0],gPoints[1]);
                        break;
                    case VisComponentType.InfiniteHorizontalLine:
                        g.DrawLine(pen,0f,gPoints.First().Y,boundries.X,gPoints.First().Y);
                        break;
                    case VisComponentType.Curve:
                        g.DrawCurve(pen, gPoints.ToArray());
                        break;
                    default:
                        throw new NotImplementedException("{nameof(item.ItemType)} is not defined for OnPaint.");
                }
            }
        } 
        
        
    }
}