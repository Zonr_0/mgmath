﻿
namespace MGMathVisualization.Ui
{
    partial class DebugVerticalControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.debugVerticalTableLayout = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonStep = new System.Windows.Forms.Button();
            this.buttonContinue = new System.Windows.Forms.Button();
            this.buttonDebugExpand = new System.Windows.Forms.Button();
            this.debugMessagesLabel = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.debugVerticalTableLayout.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // debugVerticalTableLayout
            // 
            this.debugVerticalTableLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.debugVerticalTableLayout.ColumnCount = 1;
            this.debugVerticalTableLayout.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.debugVerticalTableLayout.Controls.Add(this.tableLayoutPanel1, 0, 2);
            this.debugVerticalTableLayout.Controls.Add(this.debugMessagesLabel, 0, 0);
            this.debugVerticalTableLayout.Controls.Add(this.treeView1, 0, 1);
            this.debugVerticalTableLayout.Location = new System.Drawing.Point(0, 0);
            this.debugVerticalTableLayout.Name = "debugVerticalTableLayout";
            this.debugVerticalTableLayout.RowCount = 3;
            this.debugVerticalTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.debugVerticalTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.debugVerticalTableLayout.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.debugVerticalTableLayout.Size = new System.Drawing.Size(200, 400);
            this.debugVerticalTableLayout.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.buttonStep, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonContinue, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.buttonDebugExpand, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 343);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(194, 54);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // buttonStep
            // 
            this.buttonStep.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonStep.Location = new System.Drawing.Point(5, 3);
            this.buttonStep.Name = "buttonStep";
            this.buttonStep.Size = new System.Drawing.Size(84, 21);
            this.buttonStep.TabIndex = 0;
            this.buttonStep.Text = "Step";
            this.buttonStep.UseVisualStyleBackColor = true;
            // 
            // buttonContinue
            // 
            this.buttonContinue.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonContinue.Location = new System.Drawing.Point(105, 3);
            this.buttonContinue.Name = "buttonContinue";
            this.buttonContinue.Size = new System.Drawing.Size(84, 21);
            this.buttonContinue.TabIndex = 1;
            this.buttonContinue.Text = "Continue";
            this.buttonContinue.UseVisualStyleBackColor = true;
            // 
            // buttonDebugExpand
            // 
            this.buttonDebugExpand.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.SetColumnSpan(this.buttonDebugExpand, 3);
            this.buttonDebugExpand.Location = new System.Drawing.Point(5, 30);
            this.buttonDebugExpand.Name = "buttonDebugExpand";
            this.buttonDebugExpand.Size = new System.Drawing.Size(184, 21);
            this.buttonDebugExpand.TabIndex = 2;
            this.buttonDebugExpand.Text = "Open Debug Window";
            this.buttonDebugExpand.UseVisualStyleBackColor = true;
            // 
            // debugMessagesLabel
            // 
            this.debugMessagesLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.debugMessagesLabel.AutoSize = true;
            this.debugMessagesLabel.Location = new System.Drawing.Point(100, 0);
            this.debugMessagesLabel.Name = "debugMessagesLabel";
            this.debugMessagesLabel.Size = new System.Drawing.Size(97, 15);
            this.debugMessagesLabel.TabIndex = 1;
            this.debugMessagesLabel.Text = "Debug Hook Log";
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeView1.Location = new System.Drawing.Point(3, 18);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(194, 319);
            this.treeView1.TabIndex = 2;
            // 
            // DebugVerticalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.debugVerticalTableLayout);
            this.MinimumSize = new System.Drawing.Size(200, 400);
            this.Name = "DebugVerticalControl";
            this.Size = new System.Drawing.Size(200, 400);
            this.debugVerticalTableLayout.ResumeLayout(false);
            this.debugVerticalTableLayout.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel debugVerticalTableLayout;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonStep;
        private System.Windows.Forms.Button buttonContinue;
        private System.Windows.Forms.Button buttonDebugExpand;
        private System.Windows.Forms.Label debugMessagesLabel;
        private System.Windows.Forms.TreeView treeView1;
    }
}
