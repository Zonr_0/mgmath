using System;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;
using MGMath.FrontlineTools.Voronoi;
using MGMathVisualization.Ui;

namespace MGMathVisualization
{
    static class Program
    {
        /// <summary>
        ///  The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            VisState visState = new VisState();
            Application.SetHighDpiMode(HighDpiMode.SystemAware);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MgMathVisualDebuggerRootForm(visState));
            
        }

        private static async void BasicArcTest(VisState vis) { 
            Arc testArc = new Arc(new Vector2(3, 5), DistanceFormulas.EuclideanDistance);
            float directix = -100;
            float[] targets = Enumerable.Range(0, 800).Select(x => x -400f).ToArray();
            var points = new Vector2[targets.Length];
            for (var i = 0; i < targets.Length; ++i)
            {
                var target = targets[i];
                points[i] = new Vector2(target, (float)testArc.GetYForX(target, directix)); 
            }
            vis.AddPoint("focus",testArc.Focus, Color.DarkBlue, 2);
            vis.AddInfiniteHorizontalLine("directix",directix,Color.Coral);
            vis.AddCurve("arc",points,Color.Firebrick);
        }
    }

    class CoordinateManager
    {
        public Vector2 Origin;

        public CoordinateManager(Vector2 origin)
        {
            Origin = origin;
        }
        public PointF CtoG(Vector2 cartesianPoint) => new PointF(XtoG(cartesianPoint.X), YtoG(cartesianPoint.Y));

        public Vector2 GtoC(PointF graphicPoint) => new Vector2(XtoC(graphicPoint.X), YtoC(graphicPoint.Y));
        public float XtoG(float x) => x + Origin.X;
        public float YtoG(float y) => -y + Origin.Y;
        public float XtoC(float x) => x - Origin.X;
        public float YtoC(float y) => -y - Origin.Y;
    }
}