﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Windows.Forms;

namespace MGMathVisualization
{
    public enum VisComponentType
    {
        Point,
        Line,
        InfiniteHorizontalLine,
        Curve
    }
    public class VisState : IEnumerable
    {
        private Dictionary<string, VisComponent> _dict;
        private List<VisComponent> _components;

        public VisState()
        {
            _dict = new Dictionary<string, VisComponent>();
            _components = new List<VisComponent>();
        }

        // TODO: Implement clear state method.

        public void AddComponent(VisComponent newComponent)
        {
            _components.Add(newComponent);
            _dict.Add(newComponent.Key, newComponent);
        }

        public void AddComponent(string key, List<Vector2> points, VisComponentType type, Color? color = null, int width = 1)
        {
            AddComponent(new VisComponent(key, points, type, color, width));
        }

        public void AddInfiniteHorizontalLine(string key, float y, Color? color = null, int width = 1)
        {
            var list = new List<Vector2> { new Vector2(0, y) };
            AddComponent(key, list, VisComponentType.InfiniteHorizontalLine, color, width);
        }

        public void AddPoint(string key, Vector2 point, Color? color = null, int width = 1)
        {
            var list = new List<Vector2> { point };
            AddComponent(key, list, VisComponentType.Point, color, width);
        }

        public void AddCurve(string key, List<Vector2> points, Color? color = null, int width = 1)
        {
            AddComponent(key, points, VisComponentType.Curve, color, width);
        }
        
        public void AddCurve(string key, Vector2[] points, Color? color = null, int width = 1)
        {
            if (points == null) throw new ArgumentNullException(nameof(points));
            if (points.Length == 0) throw new ArgumentException("Value cannot be an empty collection.", nameof(points));
            AddComponent(key, points.ToList(), VisComponentType.Curve, color, width);
        }

        /// <summary>
        /// Adds a mathematical vector that originates from the origin.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="vec"></param>
        /// <param name="color"></param>
        /// <param name="width"></param>
        public void AddVectorOrigin(string key, Vector2 vec, Color? color = null, int width = 1)
        {
            var list = new List<Vector2> { Vector2.Zero, vec };
            AddComponent(key, list, VisComponentType.Line, color, width);
        }

        /// <summary>
        /// Adds a mathematical vector that originates at start and travels in the direction specified by direction.
        /// It is optionally scaled by scaling. This is useful if, for example our direction vector is normalized and
        /// we want to scale it to a given length/magnitude.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="start"></param>
        /// <param name="direction"></param>
        /// <param name="scaling"></param>
        /// <param name="color"></param>
        /// <param name="width"></param>
        public void AddVector(string key, Vector2 start, Vector2 direction, float scaling = 1.0f, Color? color = null,
            int width = 1)
        {
            var list = new List<Vector2> { start, start + scaling * direction };
            AddComponent(key, list, VisComponentType.Line, color, width);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }

        public VisComponentEnum GetEnumerator()
        {
            // I feel like I should just be able to return the list<t> enumerator here.
            return new VisComponentEnum(_components);
        }
    }

    public class VisComponent
    {
        public string Key;
        public Color ItemColor;
        public VisComponentType ItemType;
        public int Width;
        public List<Vector2> Points;

        public VisComponent(string key, List<Vector2> points, VisComponentType type, Color? color = null, int width = 1)
        {
            Key = key;
            Points = points;
            ItemType = type;
            ItemColor = color ?? Color.Black;
            Width = width;
        }
    }

    public class VisComponentEnum : IEnumerator
    {
        private List<VisComponent> _components;
        private int _index = -1;

        public VisComponentEnum(List<VisComponent> components)
        {
            _components = components;
        }

        public bool MoveNext()
        {
            return (++_index < _components.Count);
        }

        public void Reset()
        {
            _index = -1;
        }

        object IEnumerator.Current => Current;

        public VisComponent Current
        {
            get
            {
                try
                {
                    return _components[_index];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }
    }
}