# MGMath
MGMath is a class library designed to provide solutions to math-y or complicated problems that might be needed for a game, with a focus on milsim applications. Its planned first feature is a frontline generator which takes a large list of units with locations from two sides and outputs a series of points to define the frontline between the two sides and any 'pockets' that may exist. It is currently in a state of active development and is not ready for active use.

This library was created to provide a solution for a [particular feature](https://gitlab.com/overlordbot/srs-bot/-/issues/42) for the Hoggit SRS Bot.

## State
MGMath is in active development and is not ready for use yet. Almost everything public facing will change, from signatures, to namespaces, to access modifiers, so even if there is some bit of functionality you would like to use, please hold off. Its probably broken or not implemented right now anyway.

## Installation/Use
Don't.

## Security
Although this library makes no use of any high risk calls or requires any special permissions, it is important to note that despite being written by somebody with 'Security' in their job title, it hasn't actually seen any formal code review or thorough security testing.

## Acknowledgements
Although I've always loved math and found it interesting, I was always a poor student at best. As such, this library would not be possible without the writings or code examples of the following people:
* Jacques Huenis - For an amazing [write-up of Fortune's Algorithm](https://jacquesheunis.com/post/fortunes-algorithm/) and example C++ implementation.
* Bruce Dawson - I thought I knew all the dark depths of floating points, but Dawson's [series on floating point](https://jacquesheunis.com/post/fortunes-algorithm/) are a godsend and a deep dive into madness.
