﻿using System;

namespace MGMath.misc
{
    public static class MathUtil
    {
        /// <summary>
        /// Simple relative float comparison for two floats that are not zero. Does not handle most complex cases.
        /// Because there are an infinite number of points between two rational numbers and we obviously can't represent
        /// an uncountably infinite range of values in memory, floating point precision does not work as we might
        /// expect. To make a long complicated story short, the 'space' between each representable number grows larger
        /// the farther away we are from zero. One of the many consequences of this is that even with a relatively
        /// small number of arithmetic on a floating point value, we can quickly reach numbers that are very close, but
        /// not quite equal to the numbers we would otherwise expect. Traditionally, we might use a fixed epsilon
        /// comparison, (which is still our best choice when comparing against zero), but this breaks down with other
        /// values. The solution is to base our comparison on the number of 'steps' away from our expected value.
        /// </summary>
        /// <param name="a">First float to be compared</param>
        /// <param name="b">Second float to be compared</param>
        /// <param name="maxSteps">Number of 'steps' between values at a to accept a difference.</param>
        /// <returns></returns>
        public static bool RelFloatEquality(float a, float b, int maxSteps)
        {
            var relativeEpsilon = a;
            for (int i = 0; i < maxSteps; ++i)
            {
                MathF.BitIncrement(relativeEpsilon);
            }

            relativeEpsilon = MathF.Abs(relativeEpsilon);
            return MathF.Abs(b - a) <= relativeEpsilon;

        }
    }
}