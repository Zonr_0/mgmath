﻿using System;

namespace MGMath.misc
{
    /// <summary>
    /// Class for functions to deal with the insanity that is properly comparing floats. This is taken nearly verbatim
    /// from Bruce Dawson's blog and example code on the subject.
    /// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
    /// The original code and this file is licensed under the Apache License. It is a modified selection from
    /// https://github.com/randomascii/blogstuff/blob/main/FloatingPoint/CompareAsInt/CompareAsInt.cpp
    /// translated into C#. 
    /// </summary>
    public static class FMath
    {
        /// <summary>
        /// A relative equals comparison function. Used for comparisons between two non-zero numbers. Does not account
        /// or check for infinity, NAN, or subnormal sign differences.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="maxUlps"></param>
        /// <returns></returns>
        public static bool UlpsEqual(float a, float b, int maxUlps)
        {
            var aInt = BitConverter.SingleToInt32Bits(a);
            var bInt = BitConverter.SingleToInt32Bits(b);
            // Make aInt lexicographically ordered as a twos-complement int
            if (aInt < 0) { aInt = 0x7FFFFFFF - aInt + 1; } // This will intentionally overflow (I think)
            // Make bInt lexicographically ordered as a twos-complement int
            if (bInt < 0) { bInt = 0x7FFFFFFF - bInt + 1; }
            var intDiff = Math.Abs(aInt - bInt);
            // Now we can compare aInt and bInt to find out how far apart A and B are.
            return intDiff <= maxUlps;
        }
        
        public static void TestCompare(float A, float B, bool expectedResult, int maxUlps = 10) {
            bool result = UlpsEqual(A, B, maxUlps);
            if (result != expectedResult)
                Console.Write("Unexpected result final - {0}, {1}, {2}, expected {3}\n", A, B,
                    maxUlps, expectedResult ? "true" : "false");
        }

        public static void TestBattery()
        {
            float currentFloat = 2f;
            int reference = 2;
            while (currentFloat < 1000000000)
            {
                currentFloat *= 7;
                reference *= 7;
                if (currentFloat != reference)
                {
                    Console.WriteLine($"Direct equality comparison failed for {reference} with float {currentFloat}");
                }

                if (FMath.UlpsEqual(currentFloat, reference, 1))
                {
                    Console.WriteLine($"***ULP Comparison Failed*** for {reference} with float {currentFloat}");
                }
            }
            Console.WriteLine("Float tests completed");
        }
    }
}