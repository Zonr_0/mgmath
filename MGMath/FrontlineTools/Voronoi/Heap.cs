﻿using System;
using System.Collections.Generic;

namespace MGMath.FrontlineTools.Voronoi
{
    /// <summary>
    /// Simple abstract heap.
    /// </summary>
    /// <typeparam name="T">Object to store in the Heap</typeparam>
    public abstract class Heap<T>
    {
        // Lists in .net are implemented as arrays under the hood. Technically ArrayList might give a slight performance
        // advantage since Vector2 is a reference type and wibbly wobbly optimizey, but the difference is small and
        // list is the more standard style.
        protected readonly List<T> Items;
        protected int Count;
        protected readonly Comparison<T> Comparison;
        

        /// <summary>
        /// Normal Heap constructor. Requires an implementation for a comparison delegate for the object type.
        /// </summary>
        /// <param name="comparison">Determines how the priority for an item in the heap should be calculated.
        /// Comparison delegate (read: function pointer). As per the Comparison contract, takes arguments x and y.
        /// If left (x) is larger than right(y), return a value greater than 0. If left (x) is less than (y), return
        /// less than 0. Return 0 if equivalent.</param>
        /// <exception cref="NullReferenceException">Thrown if a null reference is passed as a comparison.</exception>
        public Heap(Comparison<T> comparison)
        {
            Items = new List<T>();
            if (comparison != null) Comparison = comparison;
            else
            {
                throw new NullReferenceException("Unable to utilize a null reference to a comparison.");
            }
        }

        protected int LeftIndex(int index) => 2 * index + 1;
        protected int RightIndex(int index) => 2 * index + 2;
        protected int ParentIndex(int index) => (index - 1) / 2;

        protected bool HasLeft(int index) => LeftIndex(index) < Count;
        protected bool HasRight(int index) => RightIndex(index) < Count;
        protected bool IsRoot(int index) => index == 0;

        protected T GetLeft(int index) => Items[LeftIndex(index)];
        protected T GetRight(int index) => Items[RightIndex(index)];
        protected T GetParent(int index) => Items[ParentIndex(index)];

        protected void Swap(int indexOne, int indexTwo)
        {
            (Items[indexTwo], Items[indexOne]) = (Items[indexOne], Items[indexTwo]);
        }
        
        public bool IsEmpty()
        {
            return Count == 0;
        }

        public T Peek()
        {
            if (Count == 0)
                throw new IndexOutOfRangeException();

            return Items[0];
        }

        public T Pop()
        {
            if (Count == 0)
                throw new IndexOutOfRangeException();

            var result = Items[0];
            Items[0] = Items[Count - 1];
            Count--;

            RecalculateDown();

            return result;
        }

        public void Add(T element)
        {
            if (Count == Items.Count)
                throw new IndexOutOfRangeException();

            Items[Count] = element;
            Count++;

            ReCalculateUp();
        }

        protected abstract void ReCalculateUp();
        protected abstract void RecalculateDown();
    }
    

    public class MinHeap<T> : Heap<T>
    {
        public MinHeap(Comparison<T> comparison) : base(comparison)
        {
        }

        protected override void ReCalculateUp()
        {
            var index = Count - 1;
            while (!IsRoot(index) && Comparison(Items[index],GetParent(index)) < 0)
            {
                var parentIndex = ParentIndex(index);
                Swap(parentIndex, index);
                index = parentIndex;
            }
        }

        protected override void RecalculateDown()
        {
            var index = 0;
            while (HasLeft(index))
            {
                var smallerIndex = LeftIndex(index);
                if (HasRight(index) && Comparison(GetRight(index),GetLeft(index)) < 0)
                {
                    smallerIndex = RightIndex(index);
                }

                if (Comparison(Items[smallerIndex],Items[index]) >= 0)
                {
                    break;
                }

                Swap(smallerIndex, index);
                index = smallerIndex;
            }
        }
    }
}