﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Mail;
using System.Numerics;
using static System.Single;
using static MGMath.misc.MathUtil;

// TODO: Switch from Vector2s to Vector<Double> or experiment and see if single precision is enough.

namespace MGMath.FrontlineTools.Voronoi
{
    public enum CurveType
    {
        Arc,
        Edge
        
    }

    public delegate double DistanceMethod(Vector2 point1, Vector2 point2, object? optData=null);
    public delegate void DebugCallbackLine(Vector2 point1, Vector2 point2);

    public static class DistanceFormulas
    {
        public static double EuclideanDistance(Vector2 point1, Vector2 point2, object? optData = null)
        {
            return Math.Sqrt(Math.Pow(point2.X - point1.X, 2) + Math.Pow(point2.Y - point1.Y, 2));
        }
        
        public static double ManhattanDistance(Vector2 point1, Vector2 point2, object? optData = null)
        {
            return Math.Abs(point2.X - point1.X) + Math.Abs(point2.Y - point1.Y);
        }

        public static double AdditiveWeightedDistance(Vector2 point1, Vector2 point2, object? optData = null)
        {
            var euclid = EuclideanDistance(point1, point2);
            if (optData == null) return euclid;
            var weight = (double)optData;
            return euclid - weight;
        }
    } 

    public abstract class BeachlineNode
    {
        private protected Vector2 _location; // Focus point for an arc, Start location for an edge
        private protected CurveType _type; // We could use RTTI, but this is convenient and having the enum is useful anyway
        private protected DistanceMethod _distance;

        
        
        public CurveType Type => _type;
        public BeachlineNode? Parent = null;
        public BeachlineNode? Left = null;
        public BeachlineNode? Right = null;

        protected BeachlineNode(Vector2 location, DistanceMethod? distanceMethod=null)
        {
            _distance = distanceMethod ?? DistanceFormulas.EuclideanDistance;
            _location = location;
        }
        
        protected DistanceMethod Distance => _distance;

        public Vector2 NormalizeVector(Vector2 input)
        {
            // it may be better to transform in place. Worth profiling to see.
            var magnitude = Distance(input, Vector2.Zero);
            return new Vector2((float)(input.X / magnitude), (float)(input.Y / magnitude));
        }
        // TODO: Add Logging
        // TODO: Location stuff for edges.

    }

    public class Arc : BeachlineNode
    {
        private double _weight;
        public Arc(Vector2 focus, DistanceMethod? distanceMethod=null, double weight=0) : base(focus, distanceMethod)
        {
            _type = CurveType.Arc;
            _weight = weight;
        }
        public Vector2 Focus => _location;

        public float GetYForX(float x, float sweepY)
        {
            // y = 1 / (4p) * x^2 if the vertex is at the origin.
            Vector2 vertex = new Vector2(Focus.X, Focus.Y - sweepY) / 2;
            // This is now our new origin. Lets pretend we're at 0,0 by adding this origin to everything.
            var adjustedX = x + vertex.X;
            var p = (Focus + vertex).Y;
            var y = 1 / (4 * p) * adjustedX * adjustedX;
            return y - vertex.Y; // Return us back to reality
        }
        
        public double GetYForXAlternative(double x, double sweepY)
        {
            // TODO: This doesn't work quite right, but it would be a good thing to investigate eventually.
            // It is potentially more precise and supports alternative distance metrics.
            
            /* Given a focus point at (x_f,x_y) and horizontal line (directix) at position y_d, we can imagine as we go
             left or right from x_f, we draw a dot at (x_t,y_t), such that it is equidistant from the focus point and the 
             closest location of the directix.
             
             If we trace a line between two points, and then create a second perpendicular line at the midpoint of that
             line, we get perpendicular bisector (W). Every point on this line is equidistant between the two originating
             points. If we follow this line until it reaches x_t, then its y position on the line at that point would be
             our y_t. To find this perpendicular bisector, we find a vector FT between (x_f,y_f) and (x_t,y_d). We then
             find the direction vector (B) with (x_f - y_d, x_t - x_f). We can write the resultant vector in terms of
             t as W = 1/2FT + t||B||. (1/2 FT being our midpoint offset. We normalize B so we can calculate an intercept later)
             Note also, we will define a directional vector as V=(x_t,y_d) + t||(0,1)||. Even though this is a unit vector,
             we still 'normalize' it like with B because we may be using a different distance formula (eg. for weighted
             voronoi).
             
             From there we can find the intercept between W and V. We can write this as an augmented matrix that should
             look something like this:
             |       0        ||y_f - Y_d||  1/2(x_t - x_f) |
             | 1 / ||(0,1)||  ||x_t - x_f||  1/2(y_d - x_y) |
             
             We don't particularly care about solving for x, so we can use cramer's rule to solve for y. What this means
             is that we can take the determinant of the original coefficient matrix and the determinant of the coef.
             matrix with the y column replaced with the answer column. Our y value is then det(modified) / det(original)
             or ad* - cb* / ad - cb.
             
             Alternatively, we could take the normalized W vector, and 'walk' it over to x_t. This can be calculated by
             finding the scaling value t such that t * |1,0| + x_f = x_t (or -1 if to the left). This simplifies to
             t = (x_t - x_f) / |1,0|. If we then scale W by t, we should have the vector (i.e. point) with the y coord
             we're looking for. We do have to be careful because depending on our distance method and our precision,
             we may not get exactly hit x_t!
             */

            Vector2 pointTargetBase = new Vector2((float)x, (float)sweepY); // our target x on the sweep line.
            Vector2 vecFT = pointTargetBase - Focus;
            // Problem could be that I'm subtracting vectors and using points and vectors interchangably.
            //var directionVector = new Vector2((float)sweepY - Focus.Y, (float)(x - Focus.X));
            var directionVector = new Vector2(-1 * vecFT.Y, vecFT.X);
            var directionMagnitude = Distance(Vector2.Zero, directionVector);
            var midpoint = 0.5f * vecFT + Focus; // Adding this to the focus turns it back into a point.
            // TODO: Is this correct? Eitherway, both this time and before the parabola is facing the incorrect way.
            directionVector /= (float)directionMagnitude;
            Vector2 xUnit;
            if (x < Focus.X)
            {
                xUnit = Vector2.UnitX * -1;
            }
            else if (x > Focus.X)
            {
                xUnit = Vector2.UnitX * 1;
            }
            else return sweepY;
            
            // var unitScale = Distance(xUnit, Vector2.Zero, _weight);
            var unitScale = Distance(Vector2.Zero, xUnit, _weight);
            // Might actually need to find t using the regular unit vector and then after finding our resulting vector,
            // scale t by the distance of that vector and repeat. Need to math this one out.
            var scalingFactor = (float)((x - Focus.X) / unitScale);
            var resultingVector = midpoint + scalingFactor * directionVector; 
            return resultingVector.Y;

        }
    }

    public class Edge : BeachlineNode
    {
        private readonly Vector2 _direction;  // Edges only
        public Vector2 Start => _location;
        public Vector2 Direction => _direction;

        public Edge(Vector2 start, Vector2 direction, DistanceMethod? distance = null) : base(start, distance)
        {
            _direction = direction;
            _type = CurveType.Edge;
        }
    }
    public static class BeachlineTreeOperations
    {
        private static IBeachlineDebugger _dbg = new NullBeachlineDebugger();

        public static void RegisterDebugManager(IBeachlineDebugger debugger)
        {
            _dbg = debugger;
        }

        public static BeachlineNode FindInorderPredecessor(BeachlineNode root)
        {
            //First leaf on the left
            
            // Using this might invalidate our target big O time. have to double check.
            // also double check the accuracy of this, I could swear there were some edge cases.
            if (root.Left == null) throw new InvalidOperationException("Node has no left child and thus no inorder predecessor");;
            var current = root.Left;
            while (current.Right != null)
            {
                // Debug.Assert(root.type == CurveType.Edge);
                current = current.Right;
            }
            
            Debug.Assert(current.Type == CurveType.Arc); // Double check this is true? I don't think there should be an edge with only a left child.
            return current;
        }
        
        public static BeachlineNode FindInorderSuccessor(BeachlineNode root)
        {
            //First leaf on the Right
            
            // Using this might invalidate our target big O time. have to double check.
            // also double check the accuracy of this, I could swear there were some edge cases.
            if (root.Right == null)
                throw new InvalidOperationException("Node has no right child and thus no useful inorder successor");
            var current = root.Right;
            while (current.Left != null)
            {
                // Debug.Assert(root.type == CurveType.Edge);
                current = current.Left;
            }
            
            Debug.Assert(current.Type == CurveType.Arc); // Double check this is true? I don't think there should be an edge with only a left child.
            return current;
        }

        public static BeachlineNode WalkUpLeft(BeachlineNode leftChild)
        {
            // Walk up the leftmost side of the tree until we reach the root or a right child.
            while (leftChild.Parent != null && leftChild.Parent.Left == leftChild)
            {
                leftChild = leftChild.Parent;
            }
            // Our final node should either be the only node in the tree, or the child of an edge. If we are the only
            // node in the tree, something has gone wrong.
            if (leftChild.Parent == null) throw new InvalidOperationException("Node is already the root of the tree.");
            Debug.Assert(leftChild.Parent is Edge);
            return leftChild.Parent;
        }

        public static BeachlineNode WalkUpRight(BeachlineNode rightChild)
        {
            while (rightChild.Parent != null && rightChild.Parent.Left == rightChild)
            {
                rightChild = rightChild.Parent;
            }
            // Our final node should either be the only node in the tree, or the child of an edge. If we are the only
            // node in the tree, something has gone wrong.
            if (rightChild.Parent == null) throw new InvalidOperationException("Node is already the root of the tree.");
            Debug.Assert(rightChild.Parent is Edge);
            return rightChild.Parent;
        }

        public static void DeleteSubtree(ref BeachlineNode? root)
        {
            if (root == null) return;
            DeleteSubtree(ref root.Left);
            DeleteSubtree(ref root.Right);
            root.Left = null;
            root.Right = null;
            root = null;
        }
        
        public static int CountBeachlineItems(BeachlineNode? root)
        {
            if(root == null) return 0;
            var left = CountBeachlineItems(root.Left);
            var right = CountBeachlineItems(root.Right);
            return left + right + 1;
        }

        private static bool FindEdgeArcIntersection(Edge edge, Arc arc, float sweeplineY, out Vector2? pointIntersection)
        {
            pointIntersection = null;
            // Special cases for if the edge is a completely straight vertical line
            // Note: float comparisons to zero are spooky.
            if (0f - edge.Direction.X < 2 * Epsilon)
            {
                // If our focus point is on the sweepline.
                if (RelFloatEquality(sweeplineY, arc.Focus.Y, FLOAT_TOLERANCE))
                {
                    // If the arc's focus is on the sweepline, then the arc is just a straight line too.
                    if (RelFloatEquality(edge.Start.X, arc.Focus.X, FLOAT_TOLERANCE))
                    {
                        pointIntersection = arc.Focus;
                        return true;
                    }
                    else return false; //We have two vertical lines and they will never touch.
                }

                pointIntersection = new Vector2(edge.Start.X, arc.GetYForX(edge.Start.X, sweeplineY));
                return true;
            }
            // Define the coefficients for the equation of a line that the edge uses
            // y = px + q
            float edgeSlope = edge.Direction.Y / edge.Direction.X; // p
            float edgeOffset = edge.Start.Y - edgeSlope * edge.Start.X; // q
            
            
            return false;
        }

        /// <summary>
        /// Sensitivity of equals comparisons for floats.
        /// </summary>
        private const int FLOAT_TOLERANCE = 5;

        private static Edge GetSeparatingEdge(BeachlineNode root)
        {
            // Find the first arc to our left, and the first arc to our right.
            // Arcs are always leaf nodes, and the tree is implicitly sorted by x position.
            BeachlineNode prevArc = FindInorderPredecessor(root);
            BeachlineNode nextArc = FindInorderSuccessor(root);
            Debug.Assert(prevArc is Arc && nextArc is Arc);
                
            // We want to find the common ancestor for both our previous and next arcs.
            BeachlineNode prevRoot = WalkUpLeft((prevArc));
            BeachlineNode nextRoot = WalkUpRight((nextArc));
            Debug.Assert(prevRoot != null && prevRoot == nextRoot);
            return (Edge)prevRoot;
        }

        public static Arc GetActiveArcAtPos(BeachlineNode root, float x, float sweepLineY)
        {
            BeachlineNode current = root;
            if (root == null) throw new ArgumentNullException(nameof(root));
            while (current.Type != CurveType.Arc)
            {
                var separationEdge = GetSeparatingEdge(current);
                Vector2 leftIntersect, rightIntersect;
                float intersectionPos = 0;
                // TODO: Get the intersection coordinates for both left and right

                // Use the left intersection position unless we have none, then use the right.
                var l = current.Left;
                var r = current.Right;
                current = x < intersectionPos ? current.Left! : current.Right!;
                if (current == null) throw new InvalidOperationException("Attempted to traverse off the tree. Left {l} | Right {r}");
            }
            // Our final node should be an arc.
            return (Arc)current;
        }
    }

    public interface IBeachlineDebugger
    {
        public void PublishMessage(string message);
        public void PublishVector(Vector2 toBePublished, string descriptor);
    }
    
    public class NullBeachlineDebugger : IBeachlineDebugger
    {

        public void PublishMessage(string message)
        {
            return;
        }

        public void PublishVector(Vector2 toBePublished, string descriptor)
        {
            return;
        }
    }

    public class BeachlineDebugHistory : IBeachlineDebugger
    {
        public class Event
        {
            public readonly string Message;
            public readonly object? Information;

            public Event(string message, object? information)
            {
                Message = message;
                Information = information;
            }
        }

        private List<Event> _eventHistory = new List<Event>();
        // A dictionary of function scopes paired with a start and end index
        // Shouldn't this be a stack? probably an unresolved stack and resolved stack with a new state inner class
        protected Dictionary<string, ValueTuple<int, int>> ScopeIndexes = new Dictionary<string, (int, int)>();

        public void InjectMessageIntoHistory(string message)
        {
            _eventHistory.Add(new Event(message,null));
        }

        public virtual List<Event> HistoryHead(int eventCount)
        {
            // From stack overflow: Some LINQ providers don't like negative indexes. This makes sure we just elegantly
            // get the whole list in the event the specified head size is longer than the event history.
            return _eventHistory.Skip(Math.Max(0, _eventHistory.Count() - eventCount)).ToList();
        }

        public virtual Dictionary<string,Event> HistoryDicHead(int eventCount)
        {
            // This will currently explode with any duplicate messages.
            return _eventHistory.Skip(Math.Max(0, _eventHistory.Count() - eventCount)).ToDictionary(x => x.Message);
        }

        public virtual void PublishMessage(string message)
        {
            return;
        }
        public virtual void PublishVector(Vector2 toBePublished, string message)
        {
            return;
        }
    }
}