﻿using System.Collections.Generic;
using System.Numerics;

namespace MGMath.FrontlineTools.Voronoi
{
    public class VoronoiSolver
    {
        public void FortuneAlg(List<Vector2> sites)
        {
            var state = new VoronoiState(sites);
            algorithm_setup(state);
        }

        private Vector2 Transform(Vector2 z, Vector2 closestSite)
        {
            var distance = Vector2.Distance(z, closestSite);
            return Vector2.Add(new Vector2(distance, distance), z);
        }
        
        

        private void algorithm_setup(VoronoiState state)
        {
            // sanitize max area?
            // points list says "be the sites with minimal y-coordinate, ordered by x-coordinate." X coordinate ordering
            // should happen automatically because of the priority queue?
            // load queue, priority based on x position of the sweep line of the sweep line as it occurs. pseudocode
            // says S - points, not sure why.
            state.Sites.Sort(YComparison); // Should sort in ascending order by y value
            state.PriorityQueue = new MinHeap<Vector2>(XComparison);
            foreach (var site in state.Sites)
            {
                state.PriorityQueue.Add(site);
            }

            // create initial vertical boundrary rays
        }

        private int XComparison(Vector2 x, Vector2 y)
        {
            return x.X.CompareTo(y.X);
        }
        
        private int YComparison(Vector2 x, Vector2 y)
        {
            return x.Y.CompareTo(y.Y);
        }

        private struct VoronoiState
        {
            // T - Beach Line BST - the combinatorial structure of the beach line
            // R_p - Region covered by site p
            // C_pq boundrary ray between sites p and q
            // p1,p2...pm the sites with minimal y-coordinate, ordered by x-coordinate
            public List<Vector2> Sites;
            // Q <- S - p1,p2,...,pm (priority queue, potential future events that could change the beach line structure )
            public MinHeap<Vector2>? PriorityQueue;
            public VoronoiState(List<Vector2> sites)
            {
                Sites = sites;
                PriorityQueue = null;
            }

        }
    }
}